const express = require('express');
const app = express();
const port = 3000;
const jwt = require("jsonwebtoken");
const users = require("./users.json");
const blogposts = require("./blogposts.json");
const otentikasi = require("./middleware")
app.use (otentikasi)
app.use(express.json());
app.use(express.urlencoded({extended :false}));
app.listen(port, () => {
    console.log(`Server menyala di port ${port}`);
});
app.get('/', (req, res)=> {
    res.send("Homepage")
})
//mengambil semua post user tertentu
app.get('/blog-userid/all', (req, res)=>{
    const postuserini = blogposts.filter(user => blogposts.userid == req.body.userid)
    if (postuserini.length == 0){
       
        res.status(404).send("User ini tidak ada atau belum membuat post")
        return
    }
    res.status(200).json(postuserini)
})
//end
//mengambil post tertentu
app.get('blog-userid/post-id', (req, res)=>{
    const postini = blogposts.filter(post => post.postid == req.body.postid)
    if (postini.length == 0){
        res.console.error(404);
        return
    }
    res.status(200).json(postini)
})
//end
//me-like post tertentu
app.put('blog-userid/post-id/like', (req, res) =>{
    let postyangdilike = blogposts.filter(post => post.postid == req.body.postid)
    if (postyangdilike.length == 0){
        res.console.error(404);
        return
    }
    const jumlahlikebaru = postini.likes + 1
    const params = {likes: jumlahlikebaru}
    postyangdilike = {...postyangdilike, ...params}
    blogposts.map(i => i.id == postyangdilike.id ? i : postyangdilike)
    res.send("Yayy!!")
})