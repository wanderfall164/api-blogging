const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken')
const validator = require("fastest-validator");
const users = require('./users.json');
const posting = require('./blogposts.json');
const v = new validator();
//schema untuk fastest-validator
const schemaForm = {
    email:{type: "email"},
    password:{type: "string"}
}
const schemaBlogpost = {
    title:{type: "string", min: 2},
    content:{type: "string", min: 10}
}
const check = v.compile(schemaForm);
const checkPost = v.compile(schemaBlogpost);
//middleware menggunakan fastest validator
formValidasi = (req, res, next) => {
    check(req.body);
    next()
}
postValidasi = (req, res, next) => {
    checkPost(req.body);
    next()
}
//middleware otentikasi menggunakan jwt, mengembalikan userid
cekToken = (req, res, next) => {
    let authHeader = req.headers[authorization];
    token = authHeader.split(' ')[1];
    email = jwt.verify(token, "secretkey");
    getUserID = users.filter(user => user.email == email).userid
    req.user = getUserID
    next()
}
//middleware mengecek duplikasi title
cekTitle = (req, res, next) => {
    mengecekTitle = blogposts.find(post => post.title = req.body.title)
    if (mengecekTitle){
        return res.status(400).send("Gunakan title yang unik!")
    }
    next()
}
//middleware mencegah duplikasi email user
cekEmail = (req, res, next) => {
    mengecekEmail = users.find(user => user.email = req.body.email)
    if (mengecekEmail){
        return res.status(400).send("Maaf, email ini telah terdaftar")
    }
    next()
}
//API registrasi
router.post('/registrasi', formValidasi, cekEmail, (req, res)=> {
    const {email, password} = req.body;
    const newUserid = users[users.length - 1].userid + 1;
    const userBaru = { newUserid, email, password };
    users.push(userBaru)
    res.status(201).json(userBaru)
})
router.get('/login', formValidasi, (req, res) => {
    const {email, password} = req.body;
    let userEntry = users.filter(user => user.email == email);
    if (userEntry.length == 0 || userEntry.password != password){
       return res.status(404).send("Email atau password Anda salah")
    };
    accessToken = jwt.sign(req.body.email, "secretkey");
    res.cookie(accessToken).send("login Anda berhasil")
})
router.post('/blog-userid', cekToken, postValidasi, (req, res)=>{
    const {title, content} = req.body
    const postid = blogposts[blogposts.length - 1].postid + 1
    const userid = req.user
    const likes = 0
    postBaru = { postid, userid, title, content, likes};
    blogposts.push(postBaru);
    res.status(201).send("Postingan berhasil!!")
})
module.exports = router;
